package com.example.haochenglee.uts_professional_journal;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

class JE_Main_Adapter extends RecyclerView.Adapter<JE_Main_Adapter.ViewHolder> {

    private ArrayList<Object> dataset;
    private ArrayList<Object> originally_dataset;
    private AdapterModel model;

    enum AdapterModel {

        HOME("HOME", 0),
        DELETE("DELETE", 1),
        HIDE("HIDE", 2);

        private String stringValue;
        private int intValue;

        AdapterModel(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        @Override
        public String toString() {
            return stringValue;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        Context context;
        ImageView image;
        TextView name;
        TextView description;
        TextView date;
        ConstraintLayout contentLayout;

        ViewHolder(View item) {
            super(item);
            image = item.findViewById(R.id.row_photo_image);
            name = item.findViewById(R.id.row_name);
            description = item.findViewById(R.id.row_description);
            date = item.findViewById(R.id.row_date);
            contentLayout = item.findViewById(R.id.row_content_layout);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    JE_Main_Adapter(ArrayList<Object> _dataset, AdapterModel model) {
        if (dataset == null) {
            dataset = _dataset;
        }

        if (originally_dataset == null) {
            originally_dataset = new ArrayList<>();
        }

        this.model = model;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public JE_Main_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // create a new view
        Context context = viewGroup.getContext();
        View contactView = LayoutInflater.from(context).inflate(R.layout.je_main_item_view, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(contactView);
        viewHolder.context = context;
        Log.e("onCreateViewHolder", "456");
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        System.out.println("onBindViewHolder");
        final int row = position;
        System.out.println("position => " + position);

        switch (this.model) {
            case HOME:
                System.out.println("HOME Model");
                if (dataset.size() > 0) {
                    HashMap<String, String> item = (HashMap<String, String>) dataset.get(row);
                    System.out.println("item => " + item);
                    final String _name = item.get("name");
                    final String _description = item.get("description");
                    final String _date = item.get("date");
                    final String _uuid = item.get("uuid");

                    final Context context = holder.context;
                    holder.name.setText(_name);
                    holder.description.setText(_description);
                    holder.date.setText(_date);
                    holder.contentLayout.setBackgroundResource(R.color.clear);
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setClass(context, JE_Entry_Home.class);
                            intent.putExtra("name", _name);
                            intent.putExtra("description", _description);
                            intent.putExtra("date", _date);
                            intent.putExtra("uuid", _uuid);
                            intent.putExtra("row", row);
                            Activity activity = scanForActivity(context);
                            activity.startActivityForResult(intent, 6666);
                            Log.d("position", String.valueOf(row));
                        }
                    });
                }
                break;
            case DELETE:
                System.out.println("DELETE Model");
                if (removedDataset.size() > 0) {
                    HashMap<String, String> item = (HashMap<String, String>) removedDataset.get(row);

                    final String _name = item.get("name");
                    final String _description = item.get("description");
                    final String _date = item.get("date");
                    final String _uuid = item.get("uuid");

                    final Context context = holder.context;
                    holder.name.setText(_name);
                    holder.description.setText(_description);
                    holder.date.setText(_date);
                    holder.contentLayout.setBackgroundResource(R.color.light_red);
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setClass(context, JE_Entry_Home.class);
                            intent.putExtra("name", _name);
                            intent.putExtra("description", _description);
                            intent.putExtra("date", _date);
                            intent.putExtra("uuid", _uuid);
                            context.startActivity(intent);
                            Log.d("position", String.valueOf(row));
                        }
                    });
                }
                break;
            case HIDE:
                System.out.println("HIDE Model");
                if (hiddenDataset.size() > 0) {
                    HashMap<String, String> item = (HashMap<String, String>) hiddenDataset.get(row);
                    System.out.println("item => " + item);
                    final String _name = item.get("name");
                    final String _description = item.get("description");
                    final String _date = item.get("date");
                    final String _uuid = item.get("uuid");

                    final Context context = holder.context;
                    holder.name.setText(_name);
                    holder.description.setText(_description);
                    holder.date.setText(_date);
                    holder.contentLayout.setBackgroundResource(R.color.colorPrimary);
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setClass(context, JE_Entry_Home.class);
                            intent.putExtra("name", _name);
                            intent.putExtra("description", _description);
                            intent.putExtra("date", _date);
                            intent.putExtra("uuid", _uuid);
                            context.startActivity(intent);
                            Log.d("position", String.valueOf(row));
                        }
                    });
                }
                break;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        System.out.println("getItemCount");
        switch (this.model) {
            case HOME:
                System.out.println("HOME Model");
                return dataset.size();
            case DELETE:
                System.out.println("DELETE Model");
                return removedDataset.size();
            case HIDE:
                System.out.println("HIDE Model");
                return hiddenDataset.size();
            default:
                return 0;
        }
    }

    private ArrayList<Object> removedDataset = new ArrayList<>();

    void remove(int position) {
        System.out.println("position => " + position);
        System.out.println("REMOVE BEFORE SIZE => " + dataset.size());
        notifyItemRemoved(position);

        //add removed data to removedDataset
        removedDataset.add(dataset.get(position));

        //remove data from dataset
        dataset.remove(position);
        System.out.println("REMOVE AFTER SIZE => " + dataset.size());
        resetOringinally_Dataset();
    }

    private ArrayList<Object> hiddenDataset = new ArrayList<>();

    void hidden(int position) {
        System.out.println("position => " + position);
        System.out.println("HIDDEN BEFORE SIZE => " + dataset.size());
        notifyItemRemoved(position);

        //add hided data to hiddenDataset
        hiddenDataset.add(dataset.get(position));

        //remove data from dataset
        dataset.remove(position);
        System.out.println("HIDDEN AFTER SIZE => " + dataset.size());
        resetOringinally_Dataset();
    }

    void resetOringinally_Dataset() {
        originally_dataset.clear();
        originally_dataset.addAll(dataset);
        System.out.println("originally_dataset => " + originally_dataset.size());
        notifyDataSetChanged();
    }

    void resetDataset(AdapterModel model) {
        this.model = model;
        dataset.clear();
        dataset.addAll(originally_dataset);
        System.out.println("originally_dataset => " + originally_dataset.size());
        notifyDataSetChanged();
    }

    // Filter Class
    void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        dataset.clear();

        if (charText.length() == 0) {

            //if no any char text to search on search bar, add originally data to dataset
            dataset.addAll(originally_dataset);

        } else {

            for (Object object : originally_dataset) {
                HashMap<String, String> item = (HashMap<String, String>) object;
                final String _name = item.get("name");
                if (_name.toLowerCase(Locale.getDefault()).contains(charText)) {
                    System.out.println("item => " + item);
                    dataset.add(item);
                    System.out.println("dataset added item => " + dataset);
                }
            }
        }
        notifyDataSetChanged();
    }

    // Filter Class
    void filterDate(ArrayList<String> selectedDates) {

        if (selectedDates.size() > 0) {

            dataset.clear();
            for (String date : selectedDates) {
                System.out.println("filter date1 => " + date);
                date = date.toLowerCase(Locale.getDefault());
                System.out.println("filter date2 => " + date);
                for (Object object : originally_dataset) {
                    HashMap<String, String> item = (HashMap<String, String>) object;
                    final String _date = item.get("date");
                    System.out.println("filter date3 => " + _date);
                    if (_date.toLowerCase(Locale.getDefault()).contains(date)) {
                        System.out.println("item => " + item);
                        dataset.add(item);
                        System.out.println("dataset added item => " + dataset);
                    }
                }
            }

            System.out.println("total dataset => " + dataset);
        }

        notifyDataSetChanged();
    }

    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }

//    private String[] removeNullFrom(String[] arrays) {
//        String[] finalArray;
//
//        List<String> list = new ArrayList<>();
//
//        for(String s : arrays) {
//            if(s != null && s.length() > 0) {
//                list.add(s);
//            }
//        }
//
//        finalArray = list.toArray(new String[list.size()]);
//        return  finalArray;
//    }

}
